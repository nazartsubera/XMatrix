workspace "XMatrix"
	architecture "x64"
	startproject "XMEditor"

	configurations 
	{
		"Debug",
		"Release"
	}

	

IncludeDir = {}
IncludeDir["GLFW"] = "%{wks.location}/XMatrix/ThirdParty/GLFW/include"
IncludeDir["Glad"] = "%{wks.location}/XMatrix/ThirdParty/Glad/include"
IncludeDir["spdlog"] = "%{wks.location}/XMatrix/ThirdParty/spdlog/include"
IncludeDir["glm"] = "%{wks.location}/XMatrix/ThirdParty/glm"
IncludeDir["ImGui"] = "%{wks.location}/XMatrix/ThirdParty/ImGui"
IncludeDir["stb"] = "%{wks.location}/XMatrix/ThirdParty/stb"



outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

group "Dependencies"

include "XMatrix/ThirdParty/Glad"
include "XMatrix/ThirdParty/ImGui"
include "XMatrix/ThirdParty/GLFW"
include "XMatrix/ThirdParty/stb"


group ""


include "XMatrix"
include "XMEditor"
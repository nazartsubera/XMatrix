#pragma once
#include "XMatrix/Rendering/VertexArray.h"

namespace XMatrix {

	class OpenGLVertexArray : public VertexArray
	{
	private:
		std::vector<VertexBuffer*> m_VertexBuffers;
		IndexBuffer* m_IndexBuffer;
	private:
		unsigned int m_ArrayID;
		unsigned int m_VertexBufferIndex;
	public:
		OpenGLVertexArray();
		~OpenGLVertexArray() override;

		void AddVertexBuffer(VertexBuffer* vb) override;
		void SetIndexBuffer(IndexBuffer* ib) override;

		IndexBuffer* GetIndexBuffer() const override;
		std::vector<VertexBuffer*> GetVertexBuffers() const override;

		void Bind() const override;
		void Unbind() const override;
	};

}
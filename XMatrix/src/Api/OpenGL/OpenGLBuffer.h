#pragma once
#include "XMatrix/Rendering/Buffer.h"

namespace XMatrix 
{
	class OpenGLVertexBuffer : public VertexBuffer 
	{
	private:
		unsigned int m_BufferID;
	public:
		OpenGLVertexBuffer(const void* data, unsigned int size);
		~OpenGLVertexBuffer() override;

		void Bind() const override;
		void Unbind() const override;
	};

	class OpenGLIndexBuffer : public IndexBuffer 
	{
	private:
		unsigned int m_BufferID;
		unsigned int m_Size;
	public:
		OpenGLIndexBuffer(const unsigned int* indices, unsigned int size);
		~OpenGLIndexBuffer() override;

		void Bind() const override;
		void Unbind() const override;

		unsigned int GetCount() const override;
	};


}
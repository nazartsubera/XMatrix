#include "xmpch.h"
#include "OpenGLApi.h"
#include "glad/glad.h"

namespace XMatrix
{
	void OpenGLApi::ClearImpl()
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void OpenGLApi::ClearColorImpl (const glm::vec4& color)
	{
		glClearColor(color.r, color.g, color.b, color.a);
	}

	void OpenGLApi::DrawImpl(const VertexArray* va) 
	{
		glDrawElements(GL_TRIANGLES, va->GetIndexBuffer()->GetCount(), GL_UNSIGNED_INT, nullptr);
	}
}
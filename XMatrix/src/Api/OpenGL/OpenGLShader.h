#pragma once
#include "XMatrix/Rendering/Shader.h"

namespace XMatrix 
{
	class OpenGLShader : public Shader
	{
	private:
		unsigned int m_ProgramID;
	public:
		OpenGLShader(const std::string& name);
		~OpenGLShader() override; 

		void Bind() const override;
		void Unbind() const override;

		unsigned int GetUniformLocation(const std::string& name) const override;

		unsigned int CompileShader(const std::string& source, ShaderType type) override;

		void SetUniform1i(const std::string& name, int uniform) const override;

		void SetUniform1f(const std::string& name, float uniform) const override;
		void SetUniform2f(const std::string& name, const glm::vec2& uniform) const override;
		void SetUniform3f(const std::string& name, const glm::vec3& uniform) const override;
		void SetUniform4f(const std::string& name, const glm::vec4& uniform) const override;
		void SetUniformMat4f(const std::string& name, const glm::mat4& uniform) const override;
	};
}

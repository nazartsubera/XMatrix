#pragma once
#include "XMatrix/Rendering/Texture.h"

namespace XMatrix
{

	class OpenGLTexture : public Texture
	{
	private:
		unsigned int m_TextureID;
		int m_X;
		int m_Y;
		int m_Channels;
		unsigned char* m_Buffer;



	public:
		OpenGLTexture(const std::string& filename);
		~OpenGLTexture();

		void Bind(unsigned int slot) const override;
		void Unbind() const override;
	};

}
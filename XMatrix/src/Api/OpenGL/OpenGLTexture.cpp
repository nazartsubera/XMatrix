#include "xmpch.h"
#include "OpenGLTexture.h"
#include "glad/glad.h"
#include <stb/stb_image.h>

namespace XMatrix 
{
	OpenGLTexture::OpenGLTexture(const std::string& filename) 
	{
		stbi_set_flip_vertically_on_load(true);
		m_Buffer = stbi_load(filename.c_str(), &m_X, &m_Y, &m_Channels, 4);


		glGenTextures(1, &m_TextureID);
		glBindTexture(GL_TEXTURE_2D, m_TextureID);


		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_X, m_Y, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_Buffer);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void OpenGLTexture::Bind(unsigned int slot=0) const
	{
		glActiveTexture(GL_TEXTURE0 + slot);
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
	}


	void OpenGLTexture::Unbind() const
	{
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
	}

	OpenGLTexture::~OpenGLTexture() 
	{
		glDeleteTextures(1, &m_TextureID);
	}
}
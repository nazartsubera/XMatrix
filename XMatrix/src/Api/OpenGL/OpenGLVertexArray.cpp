#include "xmpch.h"
#include "OpenGLVertexArray.h"
#include "XMatrix/Rendering/DataTypes.h"
#include "glad/glad.h"

namespace XMatrix
{
	OpenGLVertexArray::OpenGLVertexArray()
		: m_VertexBufferIndex(0)
	{
		glGenVertexArrays(1, &m_ArrayID);
		glBindVertexArray(m_ArrayID);
	}

	void OpenGLVertexArray::AddVertexBuffer(VertexBuffer* vb)
	{
		glBindVertexArray(m_ArrayID);
		vb->Bind();

		BufferLayout layout = vb->GetLayout();

		unsigned int offset = 0;

		for (BufferLayoutElement element : layout.GetElements())
		{
			glEnableVertexAttribArray(m_VertexBufferIndex);
			glVertexAttribPointer(m_VertexBufferIndex, element.Count, GetNativeDataType(element.Type), element.Normalized, layout.GetStride(), (unsigned int*)offset);

			offset += element.Count * GetSizeOf(element.Type);
			m_VertexBufferIndex++;
		}
	}

	void OpenGLVertexArray::SetIndexBuffer(IndexBuffer* ib)
	{
		glBindVertexArray(m_ArrayID);
		ib->Bind();
		m_IndexBuffer = ib;
	}

	IndexBuffer* OpenGLVertexArray::GetIndexBuffer() const
	{
		return m_IndexBuffer;
	}

	std::vector<VertexBuffer*> OpenGLVertexArray::GetVertexBuffers() const
	{
		return m_VertexBuffers;
	}

	void OpenGLVertexArray::Bind() const
	{
		glBindVertexArray(m_ArrayID);
	}

	void OpenGLVertexArray::Unbind() const
	{
		glBindVertexArray(0);
	}

	OpenGLVertexArray::~OpenGLVertexArray()
	{
	}
}
#include "xmpch.h"
#include "OpenGLShader.h"
#include "glad/glad.h"

namespace XMatrix
{

	OpenGLShader::OpenGLShader(const std::string& filename)
	{
		ShaderSource source = Parse(filename);
		
		m_ProgramID = glCreateProgram();

		unsigned int vertexID = CompileShader(source.VertexSource, ShaderType::Vertex);
		unsigned int fragmentID = CompileShader(source.FragmentSource, ShaderType::Fragment);

		glAttachShader(m_ProgramID, vertexID);
		glAttachShader(m_ProgramID, fragmentID);
		glValidateProgram(m_ProgramID);
		glLinkProgram(m_ProgramID);

		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		glUseProgram(m_ProgramID);
	}


	unsigned int OpenGLShader::CompileShader(const std::string& source, ShaderType type)
	{

		int sourceSize = source.length();
		char* const src = new char[source.length()];
		source.copy(src, sourceSize);


		std::string shaderName = (type == ShaderType::Vertex ? "Vertex" : "Fragment");

		unsigned int shader = glCreateShader(type == ShaderType::Vertex ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER);
		glShaderSource(shader, 1, (const GLchar* const*)&src, &sourceSize);
		glCompileShader(shader);

		int compilationSuccess;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &compilationSuccess);

		if (!compilationSuccess)
		{
			int errorSize;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errorSize);
			char* errorBuffer = new char[errorSize];
			glGetShaderInfoLog(shader, errorSize, &errorSize, errorBuffer);

			XM_INFO("Failed to compile " + shaderName + " Shader : \n" + errorBuffer);
		}
		else
		{
			XM_INFO(shaderName + " compiled successfully");
		}

		return shader;
	}

	void OpenGLShader::SetUniform1i(const std::string& name, int uniform) const
	{
		glUniform1i(GetUniformLocation(name), uniform);
	}

	unsigned int OpenGLShader::GetUniformLocation(const std::string& name) const
	{
		return glGetUniformLocation(m_ProgramID, name.c_str());
	}

	void OpenGLShader::SetUniform4f(const std::string& name, const glm::vec4& uniform) const
	{
		glUniform4f(GetUniformLocation(name), uniform.x, uniform.y, uniform.z, uniform.w);
	}

	void OpenGLShader::SetUniform3f(const std::string& name, const glm::vec3& uniform) const
	{
		glUniform3f(GetUniformLocation(name), uniform.x, uniform.y, uniform.z);
	}

	void OpenGLShader::SetUniform2f(const std::string& name, const glm::vec2& uniform) const
	{
		glUniform2f(GetUniformLocation(name), uniform.x, uniform.y);
	}

	void OpenGLShader::SetUniform1f(const std::string& name, float uniform) const
	{
		glUniform1f(GetUniformLocation(name), uniform);
	}

	void OpenGLShader::SetUniformMat4f(const std::string& name, const glm::mat4& uniform) const
	{
		glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, &uniform[0][0]);
	}




	void OpenGLShader::Bind() const
	{
		glUseProgram(m_ProgramID);

	}

	void OpenGLShader::Unbind() const
	{
		glUseProgram(0);
	}

	OpenGLShader::~OpenGLShader()
	{
		glDeleteProgram(m_ProgramID);
	}
}
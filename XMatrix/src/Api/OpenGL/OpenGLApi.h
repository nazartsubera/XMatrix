#include "XMatrix/Rendering/RenderApi.h"

namespace XMatrix 
{
	class OpenGLApi : public RenderApi
	{
	protected:
		void DrawImpl(const VertexArray* va) override;
		void ClearImpl() override;
		void ClearColorImpl(const glm::vec4& color) override;
		Api GetApiImpl() override { return Api::OpenGL; }
	};
}
#pragma once
#include "XMatrix/Core.h"
#include "XMatrix/Application.h"
#include "XMatrix/Log.h"
#include "XMatrix/Window.h"
#include "XMatrix/Events/Events.h"
#include "XMatrix/ImGui/ImGuiLayer.h"


//Rendering
#include "XMatrix/Rendering/Renderer.h"
#include "XMatrix/Rendering/OrthographicCamera.h"
#include "XMatrix/Rendering/OrthographicCameraController.h"
#include "XMatrix/Rendering/RenderApi.h"
#include "XMatrix/Rendering/Texture.h"

//Scenes
#include "XMatrix/Scenes/Scene.h"
#include "XMatrix/Scenes/Entity.h"
#include "XMatrix/Scenes/Behaviour.h"

//Components
#include "XMatrix/Scenes/Components/Component.h"
#include "XMatrix/Scenes/Components/TransformComponent.h"
#include "XMatrix/Scenes/Components/CameraComponent.h"
#include "XMatrix/Scenes/Components/CameraControllerComponent.h"

//Project
#include "XMatrix/Project.h"
#pragma once
#include "XMatrix/Layer.h"
#include "imgui.h"


namespace XMatrix 
{
	/*
	* ImGuiLayer
	* 
	* Description:
	*	Overrides Layer and makes it compatable with ImGui rendering
	*/

	class ImGuiLayer : public Layer
	{
	private:
		ImGuiLayer();

	public:
		void OnUpdate() override;
		void OnAttach() override;
		void OnDetach() override;

		void BeginFrame();
		void EndFrame();


		inline ImGuiContext* GetContext() { return m_Context; }
		static inline ImGuiLayer* Get() { static ImGuiLayer instance; return &instance; }


	private:
		ImGuiContext* m_Context;
		float m_Time = 0.0f;
	};

}
#pragma once
#include "spdlog/spdlog.h"

namespace XMatrix
{
	/*
	* Log
	* 
	* Description:
	*	Incapsulates logging functions.
	* 
	*/

	class Log
	{
	public:
		static void Init();
		static std::shared_ptr<spdlog::logger> GetUserLogger();
		static std::shared_ptr<spdlog::logger> GetCoreLogger();

	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_UserLogger;
	};
	
}

#define XM_INFO(__MSG__) ::XMatrix::Log::GetCoreLogger()->info(__MSG__)
#define XM_WARN(__MSG__) ::XMatrix::Log::GetCoreLogger()->warn(__MSG__)
#define XM_ERROR(__MSG__) ::XMatrix::Log::GetCoreLogger()->error(__MSG__)
#define XM_TRACE(__MSG__) ::XMatrix::Log::GetCoreLogger()->trace(__MSG__)
#define XM_CRITICAL(__MSG__) ::XMatrix::Log::GetCoreLogger()->critical(__MSG__)

#define XM_CORE_INFO(__MSG__) ::XMatrix::Log::GetCoreLogger()->info(__MSG__)
#define XM_CORE_WARN(__MSG__) ::XMatrix::Log::GetCoreLogger()->warn(__MSG__)
#define XM_CORE_ERROR(__MSG__) ::XMatrix::Log::GetCoreLogger()->error(__MSG__)
#define XM_CORE_TRACE(__MSG__) ::XMatrix::Log::GetCoreLogger()->trace(__MSG__)
#define XM_CORE_CRITICAL(__MSG__) ::XMatrix::Log::GetCoreLogger()->critical(__MSG__)
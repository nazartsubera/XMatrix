#pragma once
#include "Layer.h"

namespace XMatrix 
{
	/*
	* LayerStack
	* 
	* Description:
	*	Data structure used to operate and contain layers.
	*/

	class LayerStack
	{
		friend class Application;
	private:
		std::vector<Layer*> m_Vector;
	public:
		LayerStack();
		~LayerStack();

		void PushLayer(Layer* layer);
		void PopLayer();
		void RemoveLayer(Layer* layer);

		inline std::vector<Layer*> GetVector() const { return m_Vector; }

	};

}
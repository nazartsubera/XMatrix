#include "xmpch.h"
#include "Input.h"
#include "XMatrix/Application.h"
#include "GLFW/glfw3.h"

namespace XMatrix
{
	bool Input::AreControlsLocked = false;

	bool Input::IsKeyPressed(KeyCode key)
	{
		return glfwGetKey(Application::Get()->GetWindow()->GetHandle(), key) == GLFW_PRESS;
	}

	bool Input::IsKeyReleased(KeyCode key)
	{
		return glfwGetKey(Application::Get()->GetWindow()->GetHandle(), key) == GLFW_RELEASE;
	}

	bool Input::IsMouseButtonDown(MouseButton button)
	{
		return glfwGetMouseButton(Application::Get()->GetWindow()->GetHandle(), button) == GLFW_PRESS;
	}
	bool Input::IsMouseButtonUp(MouseButton button)
	{
		return glfwGetMouseButton(Application::Get()->GetWindow()->GetHandle(), button) == GLFW_RELEASE;
	}

	glm::vec2 Input::GetMousePosition()
	{
		double xPos;
		double yPos;
		glfwGetCursorPos(Application::Get()->GetWindow()->GetHandle(), &xPos, &yPos);
		return glm::vec2(xPos, yPos);
	}
}
#pragma once
#include "xmpch.h"
#include "Events/Events.h"
#include "Log.h"

namespace XMatrix
{

	/*
	* Layer
	* 
	* Description:
	*	Event handling abstraction.
	* 
	*/

	class Layer
	{
	private:
		std::string m_Name;
	public:
		Layer(const std::string& name = "Layer");
		virtual ~Layer();

		virtual void OnEvent(Event& e) {}
		virtual void OnAttach() {}
		virtual void OnDetach() {}
		virtual void OnUpdate() {}

		virtual void OnImGuiRender() {}
	};
}
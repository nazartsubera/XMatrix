#pragma once
#include "Log.h"
#include <memory>

#ifdef XM_ENABLE_ASSERTS
#define XM_CORE_ASSERT(x, msg) if (!x) { XM_CORE_CRITICAL(msg); __debugbreak(); }
#define XM_ASSERT(x, msg) if (!x) { XM_CRITICAL(msg); __debugbreak(); }
#else
#define XM_CORE_ASSERT(...)
#define XM_ASSERT(...)
#endif


#define BIND_EVENT_FN(fn) [this](auto&&... args) -> decltype(auto) { return this->fn(std::forward<decltype(args)>(args)...); }
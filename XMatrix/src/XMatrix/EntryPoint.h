#pragma once
#include "Log.h"
#include "Application.h"
#include "Project.h"


/*
* Entry Point
*
* Description:
*	Implements program entry point
*	so user doesn't need to create one.
*
*/

#if defined(XMEDITOR_APPLICATION)

extern XMatrix::Application* XMatrix::CreateApplication();

int main() 
{
	XMatrix::Log::Init();
	XMatrix::Application* application = XMatrix::CreateApplication();
	application->Run();
	
	delete application;
}
#elif defined(XMPROJECT_APPLICATION)

extern XMatrix::Project* XMatrix::CreateProject();


BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    XMatrix::Project* project = XMatrix::CreateProject();
    project->OnBegin();

    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}


#endif
#pragma once

namespace XMatrix
{
	class Project
	{
	private:
		static Project* s_Instance;
	public:
		Project();
		~Project();

		virtual void OnUpdate() {};


		static void LoadProject(const std::string& path);
		inline static Project* Get() { return s_Instance; }
	};

	Project* CreateProject();
}

#include "xmpch.h"
#include "File.h"
#include "XMatrix/Log.h"

namespace XMatrix 
{
	void File::Write(std::string data) const
	{
		std::ofstream stream;
		stream.open(m_Name);

		if (!stream.is_open()) XM_INFO("File " + m_Name + " doesn't exist");

		stream << data;
	}

	std::string File::Read() const
	{
		std::string data;
		std::ifstream stream;
		stream.open(m_Name);

		if (!stream.is_open()) XM_INFO("File " + m_Name + " doesn't exist");
		

		for (char tmp; stream.get(tmp); )
		{
			data += tmp;
		}

		return data;

	}

}
#pragma once
#include "xmpch.h"

namespace XMatrix
{
	/*
	* File
	* Description:
	*	Incapsulates operation system file functions
	*/

	class File
	{
	private:
		std::string m_Name;
	public:
		File() {};
		File(const std::string& name) : m_Name(name) {}

		void Open(const std::string& name)
		{
			m_Name = name;
		}

		std::string Read() const;
		void Write(std::string data) const;
	};

	namespace FileUtil 
	{
		static std::string Read(const std::string& filename)
		{
			File file(filename);
			return file.Read();
		}


		static void Write(const std::string& filename, const std::string& data)
		{
			File file(filename);
			file.Write(data);
		}
	}
};


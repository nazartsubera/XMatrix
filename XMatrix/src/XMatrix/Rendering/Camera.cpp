#include "xmpch.h"
#include "Camera.h"
#include "OrthographicCamera.h"

namespace XMatrix 
{
	Camera* Camera::Create(CameraType type) 
	{
		switch (type)
		{
		case CameraType::OrthographicCamera: return new OrthographicCamera();
		case CameraType::PerspectiveCamera: XM_INFO("XMatrix does not support perspective cameras yet");
		}
	}
}
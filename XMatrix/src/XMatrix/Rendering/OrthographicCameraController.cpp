#include "xmpch.h"
#include "OrthographicCameraController.h"
#include "XMatrix/Input/Input.h"

namespace XMatrix 
{
	void OrthographicCameraController::OnEvent(Event& e) 
	{
		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<MouseMovedEvent>(BIND_EVENT_FN(OnMouseMove));
		dispatcher.Dispatch<MouseButtonEvent>(BIND_EVENT_FN(OnMouseButton));
	}

	void OrthographicCameraController::OnUpdate()
	{

		if (Input::IsKeyPressed(KeyCode::A)) m_Camera->Translate(glm::vec3(-m_TranslationSpeed, 0.0f, 0.0f));
		if (Input::IsKeyPressed(KeyCode::D)) m_Camera->Translate(glm::vec3(m_TranslationSpeed, 0.0f, 0.0f));
		if (Input::IsKeyPressed(KeyCode::W)) m_Camera->Translate(glm::vec3(0.0f, m_TranslationSpeed, 0.0f));
		if (Input::IsKeyPressed(KeyCode::S)) m_Camera->Translate(glm::vec3(0.0f, -m_TranslationSpeed, 0.0f));
	}

	bool OrthographicCameraController::OnKey(KeyEvent& e)
	{

		return true;
	}

	bool OrthographicCameraController::OnMouseMove(MouseMovedEvent& e)
	{
		if (Input::IsMouseButtonDown(MouseButton::Wheel) && !m_Lock3DRotations)
			m_Camera->Rotate(glm::vec3(e.GetDeltaY() * m_RotationSpeed, e.GetDeltaX() * m_RotationSpeed, 0.0f));

		return true;
	}


	bool OrthographicCameraController::OnMouseButton(MouseButtonEvent& e)
	{
		return true;
	}
}
#pragma once
#include "Camera.h"
#include "glm/gtc/matrix_transform.hpp"

namespace XMatrix 
{
	class OrthographicCamera : public Camera
	{
	private:
		glm::vec3 m_Rotation;
		glm::vec3 m_Position;

		glm::mat4 m_ViewMatrix;
		glm::mat4 m_ProjectionMatrix;
		glm::mat4 m_ViewProjectionMatrix;
	private:
		void RecalculateViewMatrix();
	public:
		OrthographicCamera();
		OrthographicCamera(float left, float right, float bottom, float top);

		void SetRotation(glm::vec3 rotation) override { m_Rotation = rotation; RecalculateViewMatrix(); }
		void SetPosition(glm::vec3 position) override { m_Position = position; RecalculateViewMatrix(); }

		void Translate(glm::vec3 position) override { m_Position = m_Position + position; RecalculateViewMatrix(); }
		void Rotate(glm::vec3 rotation) override { m_Rotation = m_Rotation + rotation; RecalculateViewMatrix(); }



		inline glm::vec3 GetPosition() override { return m_Position; }
		inline glm::vec3 GetRotation() override { return m_Rotation; }

		inline glm::mat4 GetProjectionMatrix() override { return m_ProjectionMatrix; }
		inline glm::mat4 GetViewProjectionMatrix() override { return m_ViewProjectionMatrix; }
		inline glm::mat4 GetViewMatrix() override { return m_ViewMatrix; }

		CAMERA_CLASS_TYPE(OrthographicCamera)
	};
}
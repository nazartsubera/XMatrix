#include "xmpch.h"
#include "Api/OpenGL/OpenGLApi.h"
#include <glad/glad.h>


namespace XMatrix
{
	RenderApi* RenderApi::s_Instance;

	void RenderApi::Create(Api api)
	{
		switch (api) 
		{
			case Api::OpenGL:
			{
				int status = gladLoadGL();
				XM_CORE_ASSERT(status, "Failed to initialize Glad");
				s_Instance = new OpenGLApi;
			}
		}
	}
}
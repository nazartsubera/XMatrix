#pragma once

namespace XMatrix
{
	enum DataType
	{
		Float, Int, Bool, Byte, Double, uInt
	};

	unsigned int GetNativeDataType(DataType type);
	unsigned int GetSizeOf(DataType type);
}
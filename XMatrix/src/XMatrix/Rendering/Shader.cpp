#include "xmpch.h"
#include "Shader.h"
#include "Api/OpenGL/OpenGLShader.h"
#include "RenderApi.h"

namespace XMatrix 
{
	Shader* Shader::Create(const std::string& filename) 
	{
		switch (RenderApi::GetApi()) 
		{
			case RenderApi::Api::OpenGL: 
			{
				return new OpenGLShader(filename);
			}
		}
	}

	Shader::ShaderSource Shader::Parse(const std::string& filename)
	{
		std::ifstream stream(filename);
		std::string line;

		ShaderType type = ShaderType::None;
		std::stringstream ss[2];

		while (getline(stream, line))
		{
			if (line.find("#shader") != std::string::npos)
			{
				if (line.find("vertex") != std::string::npos)
					type = ShaderType::Vertex;
				if (line.find("fragment") != std::string::npos)
					type = ShaderType::Fragment;
			}
			else
			{
				ss[(int)type] << line + "\n";
			}
		}

		return { ss[0].str(), ss[1].str() };
	}
}
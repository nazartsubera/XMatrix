#pragma once
#include "VertexArray.h"
#include "Buffer.h"
#include "Shader.h"
#include "glm/glm.hpp"
#include "Camera.h"

namespace XMatrix 
{
	class Renderer
	{
	public:

		static void BeginFrame(Camera* camera);
		static void Draw(const VertexArray* va, const Shader* shader, const glm::mat4& transform);

	private:
		struct SceneData 
		{
			glm::mat4 ViewProjection;
		};

		static SceneData s_SceneData;
	};
}
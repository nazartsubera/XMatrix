#pragma once

namespace XMatrix
{

	class Texture
	{
	public:
		static Texture* Create(const std::string& filename);

		virtual void Bind(unsigned int slot) const = 0;
		virtual void Unbind() const = 0;
	};

}
#include "xmpch.h"
#include "RenderApi.h"
#include "Api/OpenGL/OpenGLVertexArray.h"
#include "VertexArray.h"

namespace XMatrix
{
	VertexArray* VertexArray::Create() 
	{
		switch (RenderApi::GetApi()) 
		{
			case RenderApi::Api::OpenGL: 
			{
				return new OpenGLVertexArray;
			}
		}
	}
}
#include "xmpch.h"
#include "Texture.h"
#include "Api/OpenGL/OpenGLTexture.h"
#include "RenderApi.h"

namespace XMatrix 
{
	Texture* Texture::Create(const std::string& filename) 
	{
		switch (RenderApi::GetApi()) 
		{
		case RenderApi::Api::OpenGL: return new OpenGLTexture(filename);
		}
	}
}
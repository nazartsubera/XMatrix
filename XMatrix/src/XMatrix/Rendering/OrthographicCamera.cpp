#include "xmpch.h"
#include "OrthographicCamera.h"

namespace XMatrix 
{

	OrthographicCamera::OrthographicCamera(float left, float right, float bottom, float top)
		:
		m_ProjectionMatrix(1.0f), m_ViewProjectionMatrix(1.0f),
		m_Position({ 0.0f, 0.0f, 0.0f }), m_Rotation(0.0f)
	{}

	OrthographicCamera::OrthographicCamera() : OrthographicCamera(-1.0f, 1.0f, -1.0f, 1.0f)
	{}


	void OrthographicCamera::RecalculateViewMatrix() 
	{
		glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), m_Rotation.x, glm::vec3(1.0f, 0.0f, 0.0f)) * glm::rotate(glm::mat4(1.0f), m_Rotation.y, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::rotate(glm::mat4(1.0f), m_Rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

		glm::mat4 transform = glm::translate(glm::mat4(1.0f), m_Position) * rotation;
		m_ViewMatrix = glm::inverse(transform);

		m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
	}
}
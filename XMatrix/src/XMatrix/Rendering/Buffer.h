#pragma once
#include "XMatrix/Core.h"
#include "DataTypes.h"

namespace XMatrix 
{

	struct BufferLayoutElement
	{
		unsigned int Count;
		DataType Type;
		bool Normalized;
	};

	class BufferLayout
	{
	private:
		unsigned int m_Stride;
		std::vector<BufferLayoutElement> m_Elements;
	public:
		BufferLayout() : m_Stride(0) {}
		BufferLayout(const std::initializer_list<BufferLayoutElement>& vector) : BufferLayout()
		{
			for (BufferLayoutElement element : vector) 
			{
				m_Elements.push_back({ element.Count, element.Type, element.Normalized });
				m_Stride += element.Count * GetSizeOf(element.Type);
			}
		}


		template <typename T>
		void Push(unsigned int count)
		{
			XM_CORE_ASSERT(false, "Type must be specified")
		}

		template <>
		void Push<float>(unsigned int count)
		{
			m_Elements.push_back({ count, DataType::Float, false });
			m_Stride += count * GetSizeOf(DataType::Float);
		}

		template <>
		void Push<double>(unsigned int count)
		{
			m_Elements.push_back({ count, DataType::Double, false });
			m_Stride += count * GetSizeOf(DataType::Double);
		}


		inline std::vector<BufferLayoutElement> GetElements() const { return m_Elements; }
		inline unsigned int GetStride() const { return m_Stride; }
	};

	/*
	* VertexBuffer
	*/

	class VertexBuffer
	{
	private:
		BufferLayout m_Layout;
	public:
		static VertexBuffer* Create(const void* data, unsigned int size);

		virtual ~VertexBuffer() {}

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		void SetLayout(BufferLayout layout)
		{
			m_Layout = layout;
		}

		BufferLayout GetLayout() const 
		{
			return m_Layout;
		}
	};

	/*
	* IndexBuffer
	*/

	class IndexBuffer
	{
	public:
		static IndexBuffer* Create(const unsigned int* data, unsigned int size);

		virtual ~IndexBuffer() {}
	public:
		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual unsigned int GetCount() const = 0;
	};
}
#include "xmpch.h"
#include "Buffer.h"
#include "Api/OpenGL/OpenGLBuffer.h"
#include "XMatrix/Log.h"
#include "RenderApi.h"


namespace XMatrix 
{
	VertexBuffer* VertexBuffer::Create(const void* data, unsigned int size)
	{
		switch (RenderApi::GetApi())
		{
			case RenderApi::Api::OpenGL:
			{
				return new OpenGLVertexBuffer(data, size);
			}
		}
	}

	IndexBuffer* IndexBuffer::Create(const unsigned int* data, unsigned int size)
	{
		switch (RenderApi::GetApi())
		{
			case RenderApi::Api::OpenGL:
			{
				return new OpenGLIndexBuffer(data, size);
			}
		}
	}

}
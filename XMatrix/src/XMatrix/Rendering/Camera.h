#pragma once
#include "glm/glm.hpp"

namespace XMatrix
{
	class Camera
	{
	public:
		enum class CameraType 
		{
			OrthographicCamera = 0,
			PerspectiveCamera = 1
		};
	public:

		static Camera* Create(CameraType type);

		virtual void SetRotation(glm::vec3 rotation) = 0;
		virtual void SetPosition(glm::vec3 position) = 0;

		virtual void Translate(glm::vec3 position) = 0;
		virtual void Rotate(glm::vec3 rotation) = 0;

		virtual CameraType GetType() = 0;


		virtual  glm::vec3 GetPosition() = 0;
		virtual  glm::vec3 GetRotation() = 0;

		virtual  glm::mat4 GetProjectionMatrix() = 0;
		virtual  glm::mat4 GetViewProjectionMatrix() = 0;
		virtual  glm::mat4 GetViewMatrix() = 0;
	};

#define CAMERA_CLASS_TYPE(type) virtual CameraType GetType() override { return CameraType::##type; }
}
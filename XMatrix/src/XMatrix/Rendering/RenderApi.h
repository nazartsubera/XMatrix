#pragma once
#include "glm/glm.hpp"
#include "VertexArray.h"

namespace XMatrix {

	//TODO : Convert RenderApi to singleton
	class RenderApi
	{
	public:
		enum class Api 
		{
			None = 0,
			OpenGL = 1
		};
	private:
		static RenderApi* s_Instance;
	protected:


		virtual void DrawImpl(const VertexArray* va) = 0;
		virtual void ClearImpl() = 0;
		virtual void ClearColorImpl(const glm::vec4& color) = 0;
		virtual Api GetApiImpl() = 0;

	public:
		static void Draw(const VertexArray* va) { Get()->DrawImpl(va); };
		static void Clear() { Get()->ClearImpl(); }
		static void ClearColor(const glm::vec4& color) { Get()->ClearColorImpl(color); }
		static Api GetApi() { return Get()->GetApiImpl(); }

		inline static RenderApi* Get() { return s_Instance; };

		static void Create(Api api);
	};

}
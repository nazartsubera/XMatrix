#include "xmpch.h"
#include "Renderer.h"
#include "RenderApi.h"

namespace XMatrix
{
	Renderer::SceneData Renderer::s_SceneData;

	void Renderer::BeginFrame(Camera* camera) 
	{
		s_SceneData.ViewProjection = camera->GetViewProjectionMatrix();
	}

	void Renderer::Draw(const VertexArray* va, const Shader* shader, const glm::mat4& transform)
	{
		shader->Bind();
		shader->SetUniformMat4f("u_Transform", transform);
		shader->SetUniformMat4f("u_ViewProjection", s_SceneData.ViewProjection);
		RenderApi::Draw(va);
	}
}
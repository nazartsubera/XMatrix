#pragma once
#include "Buffer.h"
#include "xmpch.h"

namespace XMatrix
{
	/*
	* VertexArray
	* 
	* Description:
	*	Implementation of Vertex Array Object (VAO).
	* 
	*/

	class VertexArray
	{
	public:
		static VertexArray* Create();
		virtual ~VertexArray() {}
		   
		virtual void AddVertexBuffer(VertexBuffer* vb) = 0;
		virtual void SetIndexBuffer(IndexBuffer* ib) = 0;

		virtual std::vector<VertexBuffer*> GetVertexBuffers() const = 0;
		virtual IndexBuffer* GetIndexBuffer() const = 0;

		virtual void Bind() const = 0 ;
		virtual void Unbind() const = 0;
	};
}
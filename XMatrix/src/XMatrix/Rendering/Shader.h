#pragma once
#include "xmpch.h"
#include "glm/glm.hpp"

namespace XMatrix
{
	/*
	* Shader
	*/

	class Shader
	{
	private:
		unsigned int m_ProgramID;
	public:

		struct ShaderSource
		{
			std::string VertexSource;
			std::string FragmentSource;
		};


		enum class ShaderType 
		{
			None = -1,
			Vertex = 0,
			Fragment = 1
		};

	public:
		static Shader* Create(const std::string& filename);

		virtual ~Shader() {}

		virtual void Bind() const = 0;
		virtual void Unbind() const = 0;

		virtual unsigned int GetUniformLocation(const std::string& name) const = 0;

		virtual void SetUniform1i(const std::string& name, int uniform) const = 0;

		virtual void SetUniform1f(const std::string& name, float uniform) const = 0;
		virtual void SetUniform2f(const std::string& name, const glm::vec2& uniform) const = 0;
		virtual void SetUniform3f(const std::string& name, const glm::vec3& uniform) const = 0;
		virtual void SetUniform4f(const std::string& name, const glm::vec4& uniform) const = 0;

		virtual void SetUniformMat4f(const std::string& name, const glm::mat4& uniform) const = 0;

	protected:
		static ShaderSource Parse(const std::string& filename);
		virtual unsigned int CompileShader(const std::string& source, ShaderType type) = 0;
	};

}
#pragma once
#include "OrthographicCamera.h"
#include "XMatrix/Events/Events.h"

namespace XMatrix
{

	class OrthographicCameraController
	{
	public:
		OrthographicCameraController(OrthographicCamera* camera) 
			: m_Camera(camera)
		{}



		inline OrthographicCamera* GetCamera() { return m_Camera; }

		void OnEvent(Event& e);
		void OnUpdate();
	private:
		bool OnMouseButton(MouseButtonEvent& e);
		bool OnMouseMove(MouseMovedEvent& e);
		bool OnKey(KeyEvent& e);
	private:
		OrthographicCamera* m_Camera;

		float m_TranslationSpeed = 0.05f;
		float m_RotationSpeed = 0.005f;

		bool m_Lock3DRotations = false;


	};

}
#include "xmpch.h"
#include "DataTypes.h"
#include "RenderApi.h"
#include "glad/glad.h"

namespace XMatrix
{

	unsigned int GetNativeDataType(DataType type)
	{
		XM_CORE_ASSERT((bool)RenderApi::GetApi(), "RenderApi isn't setted up");

		if (RenderApi::GetApi() == RenderApi::Api::OpenGL)
		{
			switch (type)
			{
			case DataType::Int:	return GL_INT;
			case DataType::Float: return GL_FLOAT;
			case DataType::Double: return GL_DOUBLE;
			case DataType::Byte: return GL_BYTE;
			case DataType::uInt: return GL_UNSIGNED_INT;
			}
		}
	}

	unsigned int GetSizeOf(DataType type)
	{
		switch (type)
		{
		case DataType::Int:	return sizeof(int);
		case DataType::Float: return sizeof(float);
		case DataType::Double: return sizeof(double);
		case DataType::Byte: return sizeof(unsigned char);
		case DataType::uInt: return sizeof(unsigned int);
		}
	}
}
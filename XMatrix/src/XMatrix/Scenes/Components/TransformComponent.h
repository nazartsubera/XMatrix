#pragma once
#include "Component.h"
#include "glm/glm.hpp"

namespace XMatrix 
{

	class TransformComponent : public Component
	{
	public:
		glm::vec3 Position;
		glm::vec3 Size;
		glm::vec3 Rotation;

		TransformComponent() 
			: Position(), Size(), Rotation()
		{}

		void Translate(glm::vec3 position)
		{
			Position += position;
		}

		void Scale(glm::vec3 scale)
		{
			Size = scale;
		}

		void Rotate(glm::vec3 rotation)
		{
			Rotation += rotation;
		}

		DEFINE_COMPONENT_CLASS()
		COMPONENT_CLASS_TYPE(Transform)
	};

}
#pragma once
#include "CameraComponent.h"
#include "XMatrix/Rendering/OrthographicCameraController.h"

namespace XMatrix
{
	class CameraControllerComponent : public Component
	{
	public:
		float TranslationSpeed = 0.05f;
		float RotationSpeed = 0.005f;
	private:
		CameraComponent* m_CameraComponent;
	public:
		bool OnMouseMove(MouseMovedEvent& e);
		void SetCameraComponent(CameraComponent* component);


		DEFINE_COMPONENT_CLASS()
		COMPONENT_CLASS_TYPE(CameraController)
	};
}
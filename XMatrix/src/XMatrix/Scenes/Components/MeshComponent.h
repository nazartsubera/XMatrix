#pragma once
#include "Component.h"
#include "XMatrix/Rendering/VertexArray.h"

namespace XMatrix
{

	class MeshComponent : public Component
	{
	private:
		VertexArray* m_VertexArray;
	public:
		MeshComponent() {}
		~MeshComponent() {}


		void SetVertexArray(VertexArray* va) { m_VertexArray = va; }

		VertexArray* GetVertexArray() { return m_VertexArray; }
		

		EMPTY_COMPONENT_CLASS()
		COMPONENT_CLASS_TYPE(Mesh)
	};

}
#include "xmpch.h"
#include "TransformComponent.h"
#include "MeshRendererComponent.h"
#include "XMatrix/Scenes/Entity.h"
#include "XMatrix/Rendering/Shader.h"
#include "XMatrix/Rendering/Renderer.h"
#include "glm/gtc/matrix_transform.hpp"

namespace XMatrix 
{

	void MeshRendererComponent::OnEvent(Event& e)
	{

	}

	void MeshRendererComponent::OnDestroy()
	{

	}

	void MeshRendererComponent::OnAdd()
	{
		m_Shader = Shader::Create("D:/dev/XMatrix/XMEditor/res/Shaders/Example.shader");
	}

	void MeshRendererComponent::OnUpdate()
	{
		if (Active)
		{
			TransformComponent* transform = GetComponent<TransformComponent>();
			MeshComponent* mesh = GetComponent<MeshComponent>();
			VertexArray* va = mesh->GetVertexArray();

			glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), transform->Rotation.x, glm::vec3(1.0f, 0.0f, 0.0f)) * glm::rotate(glm::mat4(1.0f), transform->Rotation.y, glm::vec3(0.0f, 1.0f, 0.0f)) * glm::rotate(glm::mat4(1.0f), transform->Rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

			Renderer::Draw(va, m_Shader, glm::translate(glm::mat4(1.0f), transform->Position) * rotation);
		}

	}
}
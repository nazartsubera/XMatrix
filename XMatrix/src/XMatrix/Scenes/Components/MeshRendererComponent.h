#pragma once
#include "MeshComponent.h"
#include "XMatrix/Rendering/Shader.h"

namespace XMatrix
{

	class MeshRendererComponent : public Component
	{
	public:
		bool Active;
	private:
		Shader* m_Shader;
	public:
		MeshRendererComponent() {}
		~MeshRendererComponent() {}

		DEFINE_COMPONENT_CLASS()
		COMPONENT_CLASS_TYPE(MeshRenderer)
		
	};

}
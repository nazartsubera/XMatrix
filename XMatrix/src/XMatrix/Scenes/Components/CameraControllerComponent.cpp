#include "xmpch.h"
#include "CameraControllerComponent.h"
#include "XMatrix/Scenes/Entity.h"
#include "XMatrix/Input/Input.h"


namespace XMatrix 
{


	void CameraControllerComponent::SetCameraComponent(CameraComponent* component)
	{
		m_CameraComponent = component;
	}

	void CameraControllerComponent::OnUpdate()
	{
		if (!Input::AreControlsLocked)
		{
			if (Input::IsKeyPressed(KeyCode::A)) m_CameraComponent->GetComponent<TransformComponent>()->Translate(glm::vec3(-TranslationSpeed, 0.0f, 0.0f));
			if (Input::IsKeyPressed(KeyCode::D)) m_CameraComponent->GetComponent<TransformComponent>()->Translate(glm::vec3(TranslationSpeed, 0.0f, 0.0f));
			if (Input::IsKeyPressed(KeyCode::W)) m_CameraComponent->GetComponent<TransformComponent>()->Translate(glm::vec3(0.0f, TranslationSpeed, 0.0f));
			if (Input::IsKeyPressed(KeyCode::S)) m_CameraComponent->GetComponent<TransformComponent>()->Translate(glm::vec3(0.0f, -TranslationSpeed, 0.0f));
		}
	}


	bool CameraControllerComponent::OnMouseMove(MouseMovedEvent& e)
	{
		if (Input::IsMouseButtonDown(MouseButton::Wheel) && !Input::AreControlsLocked)
			m_CameraComponent->GetComponent<TransformComponent>()->Rotate(glm::vec3(e.GetDeltaY() * RotationSpeed, e.GetDeltaX() * RotationSpeed, 0.0f));

		return true;
	}


	void CameraControllerComponent::OnEvent(Event& e)
	{

		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<MouseMovedEvent>(BIND_EVENT_FN(OnMouseMove));
	}

	void CameraControllerComponent::OnAdd()
	{
		m_CameraComponent = GetEntity()->GetComponent<CameraComponent>();
	}

	void CameraControllerComponent::OnDestroy()
	{
	}
}
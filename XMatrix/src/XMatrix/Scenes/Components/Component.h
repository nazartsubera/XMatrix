#pragma once
#include "XMatrix/Events/Events.h"

namespace XMatrix 
{
	class Component
	{
		friend class Entity;
	public:
		enum class ComponentType
		{
			Camera,
			CameraController,
			Transform,
			ID,
			Mesh,
			MeshRenderer,
			Shader,
			Texture,
			Physics,
			Physics2D,
		};
	private:
		Entity* m_Entity;
	public:
		virtual ~Component(){}
		
		inline Entity* GetEntity() const { return m_Entity; }

		template<class T>
		T* GetComponent() { return GetEntity()->GetComponent<T>(); }

		template<class T>
		T* AddComponent() { return GetEntity()->AddComponent<T>(); }



		virtual ComponentType GetType() = 0;
		virtual const char* GetName() = 0;

		virtual void OnUpdate() = 0;
		virtual void OnEvent(Event& e) = 0;
		virtual void OnAdd() = 0;
		virtual void OnDestroy() = 0;
	};


#define COMPONENT_CLASS_TYPE(type)	static ComponentType GetStaticType() { return ComponentType::##type; }\
									virtual ComponentType GetType() override { return GetStaticType(); } \
									virtual const char* GetName() override { return #type; }


#define DEFINE_COMPONENT_CLASS()		virtual void OnUpdate() override; \
										virtual void OnEvent(Event& e) override; \
										virtual void OnAdd() override; \
										virtual void OnDestroy() override; 

#define EMPTY_COMPONENT_CLASS()			virtual void OnUpdate() override {} \
										virtual void OnEvent(Event& e) override {} \
										virtual void OnAdd() override {} \
										virtual void OnDestroy() override {} 
}
#include "xmpch.h"
#include "IDComponent.h"


namespace XMatrix 
{
	unsigned int IDComponent::s_LastID = 0;

	IDComponent::IDComponent() 
	{
		m_ID = s_LastID + 1;
		s_LastID++;
	}

	IDComponent::~IDComponent()
	{

	}
}
#include "xmpch.h"
#include "CameraComponent.h"
#include "XMatrix/Scenes/Entity.h"

namespace XMatrix
{
	CameraComponent::~CameraComponent()
	{
		delete m_Camera;
	}

	void CameraComponent::OnUpdate()
	{
		m_Camera->SetPosition(m_Transform->Position);
		m_Camera->SetRotation(m_Transform->Rotation);
	}

	void CameraComponent::OnEvent(Event& e)
	{
	}

	void CameraComponent::OnAdd()
	{
		m_Transform = GetEntity()->GetComponent<TransformComponent>();
	}

	void CameraComponent::OnDestroy()
	{
	}
}
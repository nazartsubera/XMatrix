#include "Component.h"


namespace XMatrix
{
	class IDComponent : public Component 
	{
	private:
		static unsigned int s_LastID;
		unsigned int m_ID;
	public:
		IDComponent();
		~IDComponent();

		unsigned int GetID() { return m_ID; }
	public:
		EMPTY_COMPONENT_CLASS();
		COMPONENT_CLASS_TYPE(ID);
	};
}
#pragma once
#include "Component.h"
#include "TransformComponent.h"
#include "XMatrix/Rendering/Camera.h"

namespace XMatrix 
{

	class CameraComponent : public Component
	{
	private:
		TransformComponent* m_Transform;
		Camera* m_Camera;
	public:
		CameraComponent() : m_Camera(Camera::Create(Camera::CameraType::OrthographicCamera)) {}
		~CameraComponent();
		
		Camera* GetCamera() { return m_Camera; }


		DEFINE_COMPONENT_CLASS()
		COMPONENT_CLASS_TYPE(Camera)
	};

}
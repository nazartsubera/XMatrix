#pragma once
#include "XMatrix/Events/Event.h"
#include "Entity.h"

namespace XMatrix
{

	class Scene
	{
		friend class Entity;
	private:
		std::string m_Name;
	public:
		Scene(const std::string& name);
		~Scene();

		void OnUpdate();
		void OnEvent(Event& e);

		void OnSet();
		void OnDetach();

		Entity* FindEntity(const std::string& name);
		Entity* CreateEntity(const std::string& name);

		std::vector<Entity*> GetEntityArray();

		void DestroyEntity(Entity* entity);
		void SetCameraEntity(Entity* entity);
		



		inline std::string GetName() { return m_Name; }
	private:
		std::vector<Entity*> m_Entities;
	};

}
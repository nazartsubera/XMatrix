#include "xmpch.h"
#include "Entity.h"
#include "Components/IDComponent.h"
#include "Components/TransformComponent.h"


namespace XMatrix
{

	Entity::Entity(const std::string& name, Scene* scene) 
		: m_Name(name), m_Scene(scene)
	{
		AddComponent<XMatrix::IDComponent>();
		AddComponent<XMatrix::TransformComponent>();
		InputNameBuffer.Load(name.c_str());
	}
	
	void Entity::OnUpdate() 
	{

		for (Component* component : m_Components)
			component->OnUpdate();

		for (Behaviour* behaviour : m_Behaviours)
			behaviour->OnUpdate();
	}

	void Entity::OnEvent(Event& e) 
	{
		for (Component* component : m_Components)
			component->OnEvent(e);

		for (Behaviour* behaviour : m_Behaviours)
			behaviour->OnEvent(e);
	}

	void Entity::OnDestroy()
	{
		for (Component* component : m_Components)
			component->OnDestroy();

		for (Behaviour* behaviour : m_Behaviours)
			behaviour->OnDestroy();
	}

	bool Entity::HasBehaviour(Behaviour* behaviour)
	{
		for (Behaviour* b : m_Behaviours)
			if (b->GetName() == behaviour->GetName()) return true;

		return false;
	}

	bool Entity::HasBehaviour(const char* name)
	{
		for (Behaviour* behaviour : m_Behaviours)
			if (behaviour->GetName() == name) return true;

		return false;
	}

	void Entity::AddBehaviour(Behaviour* behaviour)
	{
		if (HasBehaviour(behaviour))
		{
			XM_INFO("Behaviour " + std::string(behaviour->GetName()) + " is already added to " + GetName() + " entity");
			return;
		}

		m_Behaviours.push_back(behaviour);
		behaviour->m_Entity = this;
		behaviour->OnAdd();
	}

	Behaviour* Entity::GetBehaviour(const char* name)
	{
		for (Behaviour* behaviour : m_Behaviours)
			if (behaviour->GetName() == name) return behaviour;

		XM_INFO("Behaviour " + std::string(name) + " is not added to " + GetName() + " entity");
		return nullptr;
	}

}

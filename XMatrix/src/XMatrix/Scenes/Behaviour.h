#pragma once


namespace XMatrix
{
	class Behaviour 
	{
		friend Entity;
	private:
		Entity* m_Entity;
	public:
		virtual const char* GetName() = 0;
		
		Entity* GetEntity() { return m_Entity; }

		
		virtual void OnUpdate() = 0;
		virtual void OnEvent(Event& e) = 0;
		virtual void OnAdd() = 0;
		virtual void OnDestroy() = 0;
	};

#define REGISTER_BEHAVIOUR_CLASS(name) virtual const char* GetName() { return #name; }

} 
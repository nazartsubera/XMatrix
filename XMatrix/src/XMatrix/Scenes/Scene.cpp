#include "xmpch.h"
#include "Scene.h"
#include "Components/CameraComponent.h"


namespace XMatrix 
{
	Scene::Scene(const std::string& name)
		: m_Name(name)
	{

	}

	void Scene::OnUpdate() 
	{
		for (Entity* entity : m_Entities)
			entity->OnUpdate();
	}

	void Scene::OnEvent(Event& e) 
	{
		for (Entity* entity : m_Entities)
			entity->OnEvent(e);
	}


	void Scene::OnSet()
	{
		
	}

	void Scene::OnDetach() 
	{
	
	}

	Entity* Scene::FindEntity(const std::string& name)
	{
		for (Entity* ent : m_Entities)
			if (ent->m_Name == name) return ent;
	}

	Entity* Scene::CreateEntity(const std::string& name)
	{
		Entity* entity = new Entity(name, this);
		m_Entities.push_back(entity);
		return  entity;
	}


	std::vector<Entity*> Scene::GetEntityArray() 
	{
		return m_Entities;
	}

	void Scene::DestroyEntity(Entity* entity)
	{
		entity->OnDestroy();

		auto iterator = std::find(m_Entities.begin(), m_Entities.end(), entity);
		m_Entities.erase(iterator);

		delete entity;
	}

	void Scene::SetCameraEntity(Entity* entity)
	{
	}

	Scene::~Scene() 
	{
	
	}
}
#pragma once
#include "Components/Component.h"
#include "XMatrix/Memory/Buffer.h"
#include "Behaviour.h"

namespace XMatrix 
{

	class Entity
	{
		friend class Scene;
	public:
		Buffer<char> InputNameBuffer;
	private:
		std::string m_Name;
		Scene* m_Scene;

		std::vector<Component*> m_Components;
		std::vector<Behaviour*> m_Behaviours;
	private:
		Entity(const std::string& name, Scene* scene);
	public:
		inline std::string GetName() { return m_Name; }
		inline Scene* GetScene() { return m_Scene; }
	public:

		void SetName(std::string name) 
		{ 
			m_Name = name; 
		}

		void OnUpdate();
		void OnEvent(Event& e);
		void OnDestroy();

		template<class T>
		T* AddComponent()
		{
			T* component = new T;
			component->m_Entity = this;
			component->OnAdd();
			m_Components.push_back(component);
			return component;
		}

		std::vector<Component*> GetAllComponents() 
		{
			return m_Components;
		}

		std::vector<Behaviour*> GetAllBehaviours()
		{
			return m_Behaviours;
		}

		template<class T>
		T* GetComponent()
		{
			for (Component* c : m_Components)
				if (c->GetType() == T::GetStaticType()) return (T*)c;
		}


		template<class T>
		bool HasComponent() 
		{
			for (Component* c : m_Components)
				if (c->GetType() == T::GetStaticType()) return true;

			return false;
		}
		

		bool HasBehaviour(Behaviour* behaviour);
		bool HasBehaviour(const char* name);


		void AddBehaviour(Behaviour* behaviour);
		Behaviour* GetBehaviour(const char* name);
	};

}
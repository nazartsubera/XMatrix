#pragma once
#include "Event.h"

namespace XMatrix 
{
	class WindowCloseEvent : public Event 
	{
	public:
		EVENT_CLASS_TYPE(WindowCloseEvent)
	};
}
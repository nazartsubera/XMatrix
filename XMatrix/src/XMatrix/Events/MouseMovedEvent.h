#pragma once
#include "Event.h"

namespace XMatrix 
{
	class Window;

	class MouseMovedEvent : public Event
	{
		friend class Window;
	private:
		double m_X;
		double m_Y;
		static double s_PrevX;
		static double s_PrevY;
	public:
		MouseMovedEvent(double x, double y)
			: m_X(x), m_Y(y)
		{}

		inline double GetPrevX() { return s_PrevX; }
		inline double GetPrevY() { return s_PrevY; }

		inline double GetDeltaX() { return GetX() - GetPrevX(); }
		inline double GetDeltaY() { return GetY() - GetPrevY(); }

		inline double GetX() { return m_X; }
		inline double GetY() { return m_Y; }

		EVENT_CLASS_TYPE(MouseMovedEvent)
	};
}
#include "xmpch.h"
#include "LayerStack.h"

namespace XMatrix
{
	LayerStack::LayerStack()
	{}

	LayerStack::~LayerStack()
	{}

	void LayerStack::PopLayer() 
	{
		m_Vector.back()->OnDetach();
		m_Vector.pop_back();
	}

	void LayerStack::RemoveLayer(Layer* layer)
	{
		m_Vector.back()->OnDetach();

		auto iterator = std::find(m_Vector.begin(), m_Vector.end(), layer);
		m_Vector.erase(iterator);
	}

	void LayerStack::PushLayer(Layer* layer) 
	{
		m_Vector.push_back(layer);
		layer->OnAttach();
	}
}
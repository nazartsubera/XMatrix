#pragma once
#include <memory>

namespace XMatrix
{

	template<typename T>
	class Buffer
	{
	private:
		T* m_Buffer;
		int m_BufferSize;
	public:
		Buffer(int size = 256)
			: m_BufferSize(size)
		{
			m_Buffer = new T[size];
			memset(m_Buffer, 0, size);
		}
	public:
		~Buffer()
		{
			delete m_Buffer;
		}

		T* GetBuffer()
		{
			return (T*)m_Buffer;
		}

		T* GetCopy()
		{
			T* cpy = new T[m_BufferSize];
			memcpy(cpy, m_Buffer, m_BufferSize);
			return cpy;
		}

		void Load(const T* data, int index=0)
		{
			for (int i = index; i < m_BufferSize; i++)
			{
				m_Buffer[i] = data[i];
			}
		}

		int GetSize() 
		{
			return m_BufferSize;
		}
	};

}
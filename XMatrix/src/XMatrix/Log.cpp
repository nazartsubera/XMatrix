#include "xmpch.h"
#include "Log.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace XMatrix 
{

	std::shared_ptr<spdlog::logger> Log::s_UserLogger;
	std::shared_ptr<spdlog::logger> Log::s_CoreLogger;

	void Log::Init()
	{
		s_CoreLogger = spdlog::stdout_color_mt("XMATRIX");
		s_UserLogger = spdlog::stdout_color_st("APP");
	}

	std::shared_ptr<spdlog::logger> Log::GetCoreLogger()
	{
		return s_CoreLogger;
	}

	std::shared_ptr<spdlog::logger> Log::GetUserLogger()
	{
		return s_UserLogger;
	}
}
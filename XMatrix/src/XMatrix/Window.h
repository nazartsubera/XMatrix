#pragma once
#include "Events/Events.h"
#include "Layer.h"

typedef struct GLFWwindow;

namespace XMatrix 
{
	struct WindowProps 
	{
		unsigned int Width;
		unsigned int Height;
		std::string Title;

		WindowProps(int width=1200, int height=840, std::string title="Window")
			: Width(width), Height(height), Title(title)
		{}
	};

	/*
	* Window
	* 
	* Description:
	*	Incapsulates GLFW window function to one abstraction.
	* 
	*/
	

	class Window
	{
		using EventCallbackFn = std::function<void(Event&)>;
	public:
		static Window* Create();
	public:
		virtual void Init(WindowProps props);
		virtual void Shutdown();
	public:
		inline unsigned int GetWidth() const { return m_Data.Width; };
		inline unsigned int GetHeight() const { return m_Data.Height; };

		virtual void OnUpdate();
		void SetEventCallback(EventCallbackFn fn) { m_Data.EventCallback = fn; }
		inline GLFWwindow* GetHandle() { return m_Handle; }
	private:
		static void MouseMovedCallback(GLFWwindow* window, double xPos, double yPos);
	private:
		struct WindowData
		{
			unsigned int Width;
			unsigned int Height;
			std::string Title;
			EventCallbackFn EventCallback;
		};

		static bool s_GlfwInitialized;
		WindowData m_Data;
		GLFWwindow* m_Handle;
	};
}
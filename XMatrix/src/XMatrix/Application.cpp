#include "xmpch.h"
#include "Application.h"
#include "Rendering/RenderApi.h"
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "Log.h"
#include "Rendering/VertexArray.h"
#include "Rendering/Shader.h"


namespace XMatrix
{

	Application* Application::s_Instance;

	Application::Application()
	{
		s_Instance = this;

		m_Window = Window::Create();
		m_Window->SetEventCallback(BIND_EVENT_FN(OnEvent));


		

		RenderApi::Create(RenderApi::Api::OpenGL);
		

		m_ImGuiLayer = ImGuiLayer::Get();
		PushLayer(m_ImGuiLayer);
	} 
	
	void Application::PushLayer(Layer* layer) 
	{
		m_LayerStack.PushLayer(layer);
	}

	void Application::PopLayer() 
	{
		m_LayerStack.PopLayer();
	}

	void Application::RemoveLayer(Layer* layer) 
	{
		m_LayerStack.RemoveLayer(layer);
	}

	void Application::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<WindowCloseEvent>(BIND_EVENT_FN(OnWindowClose));

		for (Layer* layer : m_LayerStack.m_Vector) 
			layer->OnEvent(e);
		
	}

	//Application cycle
	void Application::Run()
	{
		while (m_Running) 
		{
			for (Layer* layer : m_LayerStack.m_Vector)
				layer->OnUpdate();

			m_ImGuiLayer->BeginFrame();

			for (Layer* layer : m_LayerStack.m_Vector)
				layer->OnImGuiRender();

			m_ImGuiLayer->EndFrame();

			m_Window->OnUpdate();
		}
	}

	bool Application::OnWindowClose(WindowCloseEvent& e)
	{
		m_Running = false;
		return true;
	}


	Application::~Application() 
	{
		delete m_Window;
	}

}
#pragma once
#include "Window.h"
#include "Events/Event.h"
#include "Events/WindowCloseEvent.h"
#include "ImGui/ImGuiLayer.h"
#include "LayerStack.h"

namespace XMatrix
{
	/*
	* Application
	* 
	* Description:
	*	Abstraction of user application.
	*/

	class Application
	{
	private:
		static Application* s_Instance;
		Window* m_Window;
		ImGuiLayer* m_ImGuiLayer;
		LayerStack m_LayerStack;
		
		bool m_Running = true;

	public:
		Application();
		virtual ~Application();

	public:
		bool OnWindowClose(WindowCloseEvent& e);
		void OnEvent(Event& e);
		void Run();

		// Layers
		void RemoveLayer(Layer* layer);
		void PushLayer(Layer* layer);
		void PopLayer();


		inline static Application* Get() { return s_Instance; }
		inline Window* GetWindow() { return m_Window; }
	};


	Application* CreateApplication();
}

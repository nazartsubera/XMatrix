project "XMatrix"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir("%{wks.location}/bin/" .. outputdir .. "/%{prj.name}")
	objdir("%{wks.location}/bin-obj/" .. outputdir .. "/%{prj.name}")

	pchheader "xmpch.h"
	pchsource "src/xmpch.cpp"

	files
	{
		"src/**.h",
		"src/**.cpp",
		"src/**.hpp",
		"src/**.c"
	}

	includedirs
	{
		IncludeDir["stb"],
		IncludeDir["ImGui"],
		IncludeDir["Glad"],
		IncludeDir["GLFW"],
		IncludeDir["spdlog"],
		IncludeDir["glm"],
		"src"
	}

	filter "system:windows"
		systemversion "latest"

		defines 
		{
			"XM_PLATFORM_WINDOWS",
			"XM_DYNAMIC_LINK",
			"GLFW_INCLUDE_NONE"
		}

		links
		{
			"GLFW",
			"opengl32.lib",
			"Glad",
			"ImGui",
			"stb"
		}


	filter "configurations:Debug"
		defines 
		{ 
			"XM_DEBUG",
			"XM_ENABLE_ASSERTS"
		}
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "XM_RELEASE"
		runtime "Release"
		optimize "on"

project "stb"
    kind "StaticLib"
    language "C++"
    staticruntime "on"

    files 
    {
        "stb/**.cpp",
        "stb/**.h"
    }

    filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "on"
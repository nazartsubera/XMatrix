#shader fragment
#version 440 core

layout(location = 0) out vec4 color;

void main()
{
	color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
}

#shader vertex
#version 440 core

layout(location = 0) in vec4 position;

uniform mat4 u_Transform;
uniform mat4 u_ViewProjection;


void main()
{
	gl_Position = u_ViewProjection * u_Transform * position;
}
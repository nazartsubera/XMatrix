project "XMEditor"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"
	
	targetdir("%{wks.location}/bin/" .. outputdir .. "/%{prj.name}")
	objdir("%{wks.location}/bin-obj/" .. outputdir .. "/%{prj.name}")

	links 
	{
		"XMatrix",
		"ImGui"
	}

	files
	{
		"src/**.h",
		"src/**.cpp",
		"src/**.hpp",
		"src/**.c"
	}

	includedirs 
	{
		IncludeDir["glm"],
		IncludeDir["spdlog"],
		IncludeDir["ImGui"],
		"%{wks.location}/XMatrix/src"
	}

	defines 
	{
		"XMEDITOR_APPLICATION"
	}

	filter "system:windows"
		systemversion "latest"

		defines 
		{
			"XM_PLATFORM_WINDOWS"
		}

	
	filter "configurations:Debug"
		defines "XM_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "XM_RELEASE"
		runtime "Release"
		optimize "on"
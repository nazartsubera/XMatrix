#include "Panel.h"
#include "HierarchyPanel.h"

namespace XMEditor 
{
	Panel* Panel::Create(Panel::Type type) 
	{
		switch (type)
		{
			case Panel::Type::Hierarchy: return new HierarchyPanel;
		}
	}
}
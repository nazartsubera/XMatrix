#include "HierarchyPanel.h"
#include "../XMEditorLayer.h"
#include "XMatrix/Scenes/Components/IDComponent.h"
#include "XMatrix/Scenes/Components/MeshRendererComponent.h"
#include "XMatrix/Input/Input.h"
#include "glm/gtc/type_ptr.hpp"
#include <string>
#include "imgui.h"

namespace XMEditor
{
	HierarchyPanel::HierarchyPanel()
	{
	}

	HierarchyPanel::~HierarchyPanel() 
	{
	}

	void HierarchyPanel::OnImGuiRender() 
	{
		ImGui::Begin("Hierarchy");
		
		std::vector<XMatrix::Entity*> entities = XMEditorLayer::Get()->GetActiveScene()->GetEntityArray();


		for (XMatrix::Entity* entity : entities) 
		{
			ImGuiTreeNodeFlags flags = ((m_SelectedEntity == entity) ? ImGuiTreeNodeFlags_Selected : 0) | ImGuiTreeNodeFlags_OpenOnArrow;
			bool opened = ImGui::TreeNodeEx(entity->GetName().c_str(), flags);



			if (ImGui::IsItemClicked()) 
			{
				m_SelectedEntity = entity;
				XM_INFO(m_SelectedEntity->GetName());
			}

			if (opened) 
			{
				ImGui::TreePop();
			}
		}


		ImGui::End();

		if (m_SelectedEntity != nullptr)
			ShowProperties();
	}

	void HierarchyPanel::ShowProperties() 
	{
		ImGui::Begin("Properties");

		{
			ImGui::SameLine();
			ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth());
			ImGui::InputText("", m_SelectedEntity->InputNameBuffer.GetBuffer(), 256);
			ImGui::PopItemWidth();

			if (ImGui::IsItemDeactivatedAfterEdit())
			{
				m_SelectedEntity->SetName(m_SelectedEntity->InputNameBuffer.GetBuffer());
			}
				

		}

		ImGui::Separator();

		if (m_SelectedEntity->HasComponent<XMatrix::IDComponent>())
		{
			ImGui::Text("ID");
			ImGui::SameLine();
			ImGui::Text(std::to_string(m_SelectedEntity->GetComponent<XMatrix::IDComponent>()->GetID()).c_str());

			ImGui::Separator();
		}


		if (m_SelectedEntity->HasComponent<XMatrix::TransformComponent>()) 
		{
			ImGui::Text("Transform Component");
			ImGui::DragFloat3("Position", glm::value_ptr(m_SelectedEntity->GetComponent<XMatrix::TransformComponent>()->Position), 0.1f);
			ImGui::DragFloat3("Rotation", glm::value_ptr(m_SelectedEntity->GetComponent<XMatrix::TransformComponent>()->Rotation), 0.1f);
			ImGui::DragFloat3("Size", glm::value_ptr(m_SelectedEntity->GetComponent<XMatrix::TransformComponent>()->Size), 0.1f);

			ImGui::Separator();
		}


		if (m_SelectedEntity->HasComponent<XMatrix::CameraControllerComponent>()) 
		{
			ImGui::Text("Camera Controller Component");
			ImGui::DragFloat("Translation Speed", &m_SelectedEntity->GetComponent<XMatrix::CameraControllerComponent>()->TranslationSpeed, 0.01f);
			ImGui::DragFloat("Rotation Speed", &m_SelectedEntity->GetComponent<XMatrix::CameraControllerComponent>()->RotationSpeed, 0.01f);

			ImGui::Separator();
		}
		
		if (m_SelectedEntity->HasComponent<XMatrix::CameraComponent>())
		{
			//TODO : Add connection to camera
			static int item = 0;
			const char* const items[] = { "Orthographic" , "Perspective" };
			ImGui::Text("Camera Component");
			ImGui::Combo("View", &item, items, 2);

			ImGui::Separator();
		}

		if (m_SelectedEntity->HasComponent<XMatrix::MeshComponent>())
		{
			ImGui::Text("Mesh Component");

			XMatrix::VertexArray* va = m_SelectedEntity->GetComponent<XMatrix::MeshComponent>()->GetVertexArray();
			unsigned int count = va->GetIndexBuffer()->GetCount() / 6;

			ImGui::Text("Vertices");
			ImGui::SameLine();
			ImGui::Text(std::to_string(count).c_str());
			
			ImGui::Separator();
		}


		if (m_SelectedEntity->HasComponent<XMatrix::MeshRendererComponent>())
		{
			ImGui::Text("Mesh Renderer Component");

			ImGui::Checkbox("Active", &m_SelectedEntity->GetComponent<XMatrix::MeshRendererComponent>()->Active);


			ImGui::Separator();
		}


		for (XMatrix::Behaviour* behaviour : m_SelectedEntity->GetAllBehaviours()) 
		{
			ImGui::Text("Behaviour");
			ImGui::SameLine();
			ImGui::Text(behaviour->GetName());
			ImGui::Separator();
		}


		ImGui::End();
	}
}
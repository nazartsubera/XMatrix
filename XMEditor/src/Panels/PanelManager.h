#pragma once
#include "Panel.h"
#include <vector>

namespace XMEditor 
{

	class PanelManager
	{
	private:
		static PanelManager* s_Instance;
		PanelManager() {}
	private:
		std::vector<Panel*> m_Panels;
	private:
		Panel* AddPanelImpl(Panel::Type type);
		void PopPanelImpl(Panel::Type type);
		Panel* GetPanelImpl(Panel::Type type);
		void OnImGuiRenderImpl();
	public:
		static Panel* AddPanel(Panel::Type type) { return Get()->AddPanelImpl(type); }
		static Panel* GetPanel(Panel::Type type) { return Get()->GetPanelImpl(type); }
		static void PopPanel(Panel::Type type) { Get()->PopPanelImpl(type); }

		static void OnImGuiRender() { Get()->OnImGuiRenderImpl(); }

		static PanelManager* Get() 
		{ 
			if (s_Instance == nullptr)
				s_Instance = new PanelManager;

			return s_Instance; 
		}
	};

}
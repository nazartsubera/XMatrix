#pragma once

namespace XMEditor {

	class Panel 
	{ 
		friend class PanelManager;
	public:
		enum class Type
		{
			Hierarchy
		};
	protected:
		Panel() {}
	public:

		static Panel* Create(Type type);

		virtual void OnImGuiRender() { }
		virtual const char* GetName() = 0;
		virtual Panel::Type GetType() = 0;
	};


#define PANEL_CLASS(name)		virtual Panel::Type GetType() override { return Panel::Type::##name; } \
								virtual const char* GetName() override { return #name; }
}
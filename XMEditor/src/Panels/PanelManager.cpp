#include "PanelManager.h"
#include "HierarchyPanel.h"


namespace XMEditor 
{
	PanelManager* PanelManager::s_Instance;

	Panel* PanelManager::AddPanelImpl(Panel::Type type) 
	{
		if (GetPanelImpl(type) == nullptr)
		{
			Panel* panel = Panel::Create(type);
			m_Panels.push_back(panel);
			return panel;
		}

		return nullptr;
	}

	void PanelManager::PopPanelImpl(Panel::Type type)
	{
		for (Panel* panel : m_Panels)
			if (panel->GetType() == type) 
				m_Panels.erase(std::find(m_Panels.begin(), m_Panels.end(), panel));
	}

	Panel* PanelManager::GetPanelImpl(Panel::Type type)
	{
		for (Panel* panel : m_Panels)
			if (panel->GetType() == type) return panel;

		return nullptr;
	}

	void PanelManager::OnImGuiRenderImpl() 
	{
		for (Panel* panel : m_Panels)
			panel->OnImGuiRender();
	}
}
#pragma once
#include "XMatrix/Scenes/Entity.h"
#include "XMatrix/Memory/Buffer.h"
#include "Panel.h"

namespace XMEditor 
{

	class HierarchyPanel : public Panel
	{
	private:
		XMatrix::Entity* m_SelectedEntity = nullptr;
	public:
		HierarchyPanel();
		~HierarchyPanel();
	private:
		void ShowProperties(); 
	public:
		void OnImGuiRender() override;

		PANEL_CLASS(Hierarchy)
	};

}
#include "XMEditorLayer.h"
#include <Windows.h>
#include "Panels/PanelManager.h"
#include "XMatrix/Scenes/Components/MeshRendererComponent.h"
#include "XMatrix/Rendering/Shader.h"
#include "XMatrix/Input/Input.h"

namespace XMEditor
{

    XMEditorLayer* XMEditorLayer::s_Instance = nullptr;

    XMEditorLayer::XMEditorLayer()
    {
        ImGui::SetCurrentContext(XMatrix::ImGuiLayer::Get()->GetContext());

        s_Instance = this;


        SetActiveScene(new XMatrix::Scene("Example"));

        PanelManager::AddPanel(Panel::Type::Hierarchy);
      


        m_CameraEntity = GetActiveScene()->CreateEntity("Camera Entity");
        m_CameraEntity->AddComponent<XMatrix::CameraComponent>();
        m_CameraEntity->AddComponent<XMatrix::CameraControllerComponent>();

        m_MeshEntity = GetActiveScene()->CreateEntity("Mesh Entity");

        unsigned int indices[]
        {
            0, 1, 2,
            2, 3, 0
        };


        float vertices[]
        {
            -0.5f, -0.5f,
            0.5f, -0.5f,
            0.5f, 0.5f,
            -0.5f, 0.5f
        };


        m_Shader = XMatrix::Shader::Create("D:/dev/XMatrix/XMEditor/res/Shaders/Example.shader");

        m_PlaneVA = XMatrix::VertexArray::Create();
        m_PlaneVB = XMatrix::VertexBuffer::Create(vertices, sizeof(vertices));
        m_PlaneIB = XMatrix::IndexBuffer::Create(indices, sizeof(indices));


        m_PlaneVB->SetLayout({ {2, XMatrix::DataType::Float, false} });


        m_PlaneVA->AddVertexBuffer(m_PlaneVB);
        m_PlaneVA->SetIndexBuffer(m_PlaneIB);

        XMatrix::MeshComponent* mesh = m_MeshEntity->AddComponent<XMatrix::MeshComponent>();
        mesh->SetVertexArray(m_PlaneVA);

        XMatrix::MeshRendererComponent* renderer = m_MeshEntity->AddComponent<XMatrix::MeshRendererComponent>();

    }

    void XMEditorLayer::SetActiveScene(XMatrix::Scene* scene)
    {
        m_ActiveScene = scene;
        m_ActiveScene->OnSet();
    }


    XMatrix::Scene* XMEditorLayer::GetActiveScene()
    {
        return m_ActiveScene;
    }


    void XMEditorLayer::OnImGuiRender()
    {
        PanelManager::OnImGuiRender();
        

        XMatrix::Input::AreControlsLocked = false;

        if (ImGui::IsAnyWindowFocused())
        {
            XMatrix::Input::AreControlsLocked = true;
        }
    }

    void XMEditorLayer::OnEvent(XMatrix::Event& e)
    {
        m_ActiveScene->OnEvent(e);
    }

    void XMEditorLayer::OnUpdate()
    {

        XMatrix::RenderApi::Clear();
        XMatrix::RenderApi::ClearColor(glm::vec4(0.1f, 0.2f, 0.3f, 1.0f));


        XMatrix::Camera* camera = m_CameraEntity->GetComponent<XMatrix::CameraComponent>()->GetCamera();

        XMatrix::Renderer::BeginFrame(camera);
        m_ActiveScene->OnUpdate();
    }



    XMEditorLayer::~XMEditorLayer()
    {
        m_ActiveScene->OnDetach();
    }

}
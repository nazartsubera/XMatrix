#include "XMEditorLayer.h"
#include "XMatrix/EntryPoint.h"

namespace XMEditor 
{

	class XMEditorApp : public XMatrix::Application
	{
	public:
		XMEditorApp()
		{
			PushLayer(new XMEditorLayer());
		}
	};
}


XMatrix::Application* XMatrix::CreateApplication()
{
	return new XMEditor::XMEditorApp;
}
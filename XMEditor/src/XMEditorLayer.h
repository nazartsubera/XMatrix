#pragma once
#include "XMatrix.h"


namespace XMEditor 
{

	class XMEditorLayer : public XMatrix::Layer
	{
	private:
		static XMEditorLayer* s_Instance;
	public:
		XMEditorLayer();
		~XMEditorLayer() override;

		void OnEvent(XMatrix::Event& e) override;
		void OnUpdate() override;
		void OnImGuiRender() override;

		void SetActiveScene(XMatrix::Scene* scene);
		XMatrix::Scene* GetActiveScene();

		static XMEditorLayer* Get()
		{
			return s_Instance;
		}

	private:

		//Panels


		XMatrix::Entity* m_CameraEntity;
		XMatrix::Entity* m_MeshEntity;

		XMatrix::VertexArray* m_PlaneVA;
		XMatrix::VertexBuffer* m_PlaneVB;
		XMatrix::IndexBuffer* m_PlaneIB;

		XMatrix::Shader* m_Shader;


		XMatrix::Scene* m_ActiveScene;
	};

}